package amy.fivek;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class FiveKTest {

    public List<Runner> getAllRunners() {
        return FiveK.SINGLETON.queryRunners(r -> true, "");
    }

    @Test
    public void TestFiveK() {
        Assert.assertFalse(FiveK.SINGLETON.isStarted());
        Assert.assertEquals(0, getAllRunners().size());

        FiveK.SINGLETON.addRunner(new Runner("Amy", "Storrs", "AMES", 21, 'f'));
        FiveK.SINGLETON.addRunner(new Runner("Chuck", "Storrs", "CHCK", 23, 'm'));
        FiveK.SINGLETON.start();

        Assert.assertTrue(FiveK.SINGLETON.isStarted());
        Assert.assertEquals(2, getAllRunners().size());

        FiveK.SINGLETON.reset();

        Assert.assertFalse(FiveK.SINGLETON.isStarted());
        Assert.assertEquals(0, getAllRunners().size());
    }
}
