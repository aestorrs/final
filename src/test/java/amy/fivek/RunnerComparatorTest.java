package amy.fivek;

import org.junit.Test;
import org.junit.Assert;

import java.util.Arrays;

public class RunnerComparatorTest {

    Runner amy = new Runner("Amy", "Storrs", "#AS", 42, 'F');
    Runner chuck = new Runner("Chuck", "Storrs", "#CK", 46, 'M');
    Runner katie = new Runner("Katie", "Andrews", "KTST", 41, 'F');
    Runner zach = new Runner("Zach", "Scronce", "ZACH", 36, 'M');
    Runner bob = new Runner("Bob", "Racer", "FAST", 100, 'M');

    Runner[] runnersList = {amy, chuck, katie, zach, bob};

    @Test
    public void TestSortingByLastNameAscending() {
        Arrays.sort(runnersList, new RunnerComparator("l"));

        Runner[] expectedOrder1 = {katie, bob, zach, amy, chuck};
        Runner[] expectedOrder2 = {katie, bob, zach, chuck, amy};

        boolean matchOrder1 = Arrays.deepEquals(expectedOrder1, runnersList);
        boolean matchOrder2 = Arrays.deepEquals(expectedOrder2, runnersList);

        Assert.assertTrue(matchOrder1 || matchOrder2);
    }

    @Test
    public void TestSortingByLastNameAndFirstNameAscending() {
        Arrays.sort(runnersList, new RunnerComparator("lf"));

        Runner[] expectedOrder = {katie, bob, zach, amy, chuck};

        Assert.assertArrayEquals(expectedOrder, runnersList);
    }

    @Test
    public void TestSortingByLastNameAscendingAndFirstNameDescending() {
        Arrays.sort(runnersList, new RunnerComparator("lF"));

        Runner[] expectedOrder = {katie, bob, zach, chuck, amy};

        Assert.assertArrayEquals(expectedOrder, runnersList);
    }

    @Test
    public void TestSortingByAgeDescending() {
        Arrays.sort(runnersList, new RunnerComparator("A"));

        Runner[] expectedOrder = {bob, chuck, amy, katie, zach};

        Assert.assertArrayEquals(expectedOrder, runnersList);
    }

    @Test
    public void TestSortingByNumberDescending() {
        Arrays.sort(runnersList, new RunnerComparator("N"));

        Runner[] expectedOrder = {zach, katie, bob, chuck, amy};

        Assert.assertArrayEquals(expectedOrder, runnersList);
    }
}
