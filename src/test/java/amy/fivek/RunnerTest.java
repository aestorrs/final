package amy.fivek;

import org.junit.Assert;
import org.junit.Test;

import java.time.Duration;
import java.time.Instant;

public class RunnerTest {

    @Test
    public void TestRunnerFirstNameValidation() {
        IllegalArgumentException ex = null;

        try {
            Runner r = new Runner(null, "last", "num", 5, 'X');
        } catch (IllegalArgumentException i) {
            ex = i;
        }

        Assert.assertNotNull(ex);
        Assert.assertTrue(ex.getMessage().contains(Runner.FIRST_NAME_CAPTION));
    }

    @Test
    public void TestRunnerLastNameValidation() {
        IllegalArgumentException ex = null;

        try {
            Runner r = new Runner("first", null, "num", 5, 'X');
        } catch (IllegalArgumentException i) {
            ex = i;
        }

        Assert.assertNotNull(ex);
        Assert.assertTrue(ex.getMessage().contains(Runner.LAST_NAME_CAPTION));
    }

    @Test
    public void TestRunnerNumberValidation() {
        IllegalArgumentException ex = null;

        try {
            Runner r = new Runner("first", "last", null, 5, 'X');
        } catch (IllegalArgumentException i) {
            ex = i;
        }

        Assert.assertNotNull(ex);
        Assert.assertTrue(ex.getMessage().contains(Runner.NUMBER_CAPTION));

        try {
            Runner r = new Runner("first", "last", "NUMBER_THATS_TOO_LONG", 5, 'X');
        } catch (IllegalArgumentException i) {
            ex = i;
        }

        Assert.assertNotNull(ex);
        Assert.assertTrue(ex.getMessage().contains(Runner.NUMBER_CAPTION));
    }

    @Test
    public void TestRunnerAgeValidation() {
        IllegalArgumentException ex = null;

        try {
            Runner r = new Runner("first", "last", "num", null, 'X');
        } catch (IllegalArgumentException i) {
            ex = i;
        }

        Assert.assertNotNull(ex);
        Assert.assertTrue(ex.getMessage().contains(Runner.AGE_CAPTION));

        try {
            Runner r = new Runner("first", "last", "num", -30, 'X');
        } catch (IllegalArgumentException i) {
            ex = i;
        }

        Assert.assertNotNull(ex);
        Assert.assertTrue(ex.getMessage().contains(Runner.AGE_CAPTION));

        try {
            Runner r = new Runner("first", "last", "num", 300, 'X');
        } catch (IllegalArgumentException i) {
            ex = i;
        }

        Assert.assertNotNull(ex);
        Assert.assertTrue(ex.getMessage().contains(Runner.AGE_CAPTION));
    }

    @Test
    public void TestRunnerGenderValidation() {
        IllegalArgumentException ex = null;

        try {
            Runner r = new Runner("first", "last", "num", 5, null);
        } catch (IllegalArgumentException i) {
            ex = i;
        }

        Assert.assertNotNull(ex);
        Assert.assertTrue(ex.getMessage().contains(Runner.GENDER_CAPTION));
    }

    @Test
    public void TestRunnerIsRaceComplete() {
        Runner r = new Runner("first", "last", "num", 5, 'X');
        Assert.assertFalse(r.isRaceComplete());

        r.setTime(Duration.between(Instant.now(), Instant.now()));
        Assert.assertTrue(r.isRaceComplete());
    }

    @Test
    public void TestRunnerGetFormattedTime() {
        Runner r = new Runner("first", "last", "num", 5, 'X');

        // 66 minutes and 21 seconds
        r.setTime(Duration.ofSeconds((66 * 60) + 21));
        Assert.assertEquals("66:21", r.getFormattedTime());

        r.setTime(Duration.ofSeconds(66));
        Assert.assertEquals("01:06", r.getFormattedTime());
    }
}
