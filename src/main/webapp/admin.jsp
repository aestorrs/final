<%@ page import="amy.fivek.FiveK" %>
<%@ page import="amy.fivek.Runner" %>
<%@ page import="amy.fivek.Const" %>
<%
    Boolean isStarted = FiveK.SINGLETON.isStarted();
    String startFiveKCaption = isStarted ? "5K Already Started" : "Start 5K";
    String msg = request.getParameter(Const.SUCCESS_PARM);
    String err = request.getParameter(Const.ERR_PARM);
%>
<!DOCTYPE html>
<html>
    <head>
        <%@ include file="fragments/head.jsp" %>
    </head>
    <body>
        <%@ include file="fragments/appbar.jsp" %>
        <main class="mui-container">
            <form class="mui-form mui--text-center" action="./<%= Const.START_FIVEK_SLUG %>" method="POST">
                <button class="mui-btn mui-btn--accent mui-btn--raised" type="submit" <%= isStarted ? "disabled" : "" %>>
                    <%= startFiveKCaption %>
                </button>
            </form>
            <h2>Add Runner</h2>
            <form class="mui-form" action="./<%= Const.ADD_RUNNER_SLUG %>" method="POST">
<% if (msg != null) { %>
                <div class="mui-panel mui--text-light" style="background-color: #198754">
                    <%= msg %>
                </div>
<% } %>
<% if (err != null) { %>
                <div class="mui-panel mui--text-danger">
                    <strong>Error:</strong> <%= err %>
                </div>
<% } %>
                <div class="mui-textfield">
                    <input type="text" name="<%= Const.FNAME_PARM %>" placeholder="<%= Runner.FIRST_NAME_CAPTION %>" required maxlength="<%= Runner.FIRST_NAME_MAX_LENGTH %>">
                </div>
                <div class="mui-textfield">
                    <input type="text" name="<%= Const.LNAME_PARM %>" placeholder="<%= Runner.LAST_NAME_CAPTION %>" required maxlength="<%= Runner.LAST_NAME_MAX_LENGTH %>">
                </div>
                <ul class="mui-list--inline">
                    <li class="mui-textfield">
                        <input type="text" name="<%= Const.NUM_PARM %>" placeholder="<%= Runner.NUMBER_CAPTION %>" required maxlength="<%= Runner.NUMBER_MAX_LENGTH %>">
                    </li>
                    <li class="mui-textfield">
                        <input type="text" name="<%= Const.GENDER_PARM %>" placeholder="<%= Runner.GENDER_CAPTION %>" required maxlength="1">
                    </li
                    <li class="mui-textfield">
                        <input type="number" name="<%= Const.AGE_PARM %>" placeholder="<%= Runner.AGE_CAPTION %>" required>
                    </li>
                    <li>
                        <button type="submit" class="mui-btn mui-btn--flat mui-btn--primary">Add Runner</button>
                    </li>
                </div>
            </form>
        </main>
        <section class="mui-container">
            <h2>Runners Yet to Finish</h2>
<% request.setAttribute(Const.SHOW_FINISH_BUTTONS_ATTR, true); %>
<%@ include file="fragments/runners-table.jsp" %>
        </section>
    </body>
</html>
