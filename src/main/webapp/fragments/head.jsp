<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="//cdn.muicss.com/mui-0.10.3/css/mui.min.css" rel="stylesheet" type="text/css" />
<script src="//cdn.muicss.com/mui-0.10.3/js/mui.min.js"></script>
<style>
.mui-appbar .mui--text-light,
.mui-appbar .mui--text-accent {
    text-decoration: none;
}
img {
    max-width: 100%;
}
.mui-table tr:nth-child(even) {
    background-color: #BBDEFB;
}
</style>
