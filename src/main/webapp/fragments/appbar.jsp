<%@ page import="amy.fivek.Const" %>

<header class="mui-appbar mui--z2" style="margin-bottom: 6px">
    <div class="mui-container">
        <table style="width: 100%">
            <tr class="mui--appbar-height">
                <td class="mui--text-title">
                    <a class="mui--text-light mui--no-user-select" href="./<%= Const.HOME_SLUG %>">
                        Five<span class="mui--text-accent">K</span>
                    </a>
                </td>
                <td class="mui--text-right">
                    <ul class="mui-list--inline">
                        <li>
                            <a class="mui--text-light" href="./<%= Const.RUNNERS_SLUG %>">Runners</a>
                        </li>
                        <li>
                            <a class="mui--text-light" href="./<%= Const.ADMIN_SLUG %>">Admin</a>
                        </li>
                    </ul>
                </td>
            </tr>
        </table>
    </div>
</header>
