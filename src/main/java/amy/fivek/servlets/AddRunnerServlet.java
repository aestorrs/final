package amy.fivek.servlets;

import amy.fivek.Const;
import amy.fivek.FiveK;
import amy.fivek.Runner;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddRunnerServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            String f = req.getParameter(Const.FNAME_PARM);
            String l = req.getParameter(Const.LNAME_PARM);
            String n = req.getParameter(Const.NUM_PARM);
            Integer a = Integer.valueOf(req.getParameter(Const.AGE_PARM));
            Character g = req.getParameter(Const.GENDER_PARM).charAt(0);

            Runner r = new Runner(f, l, n, a, g);

            FiveK.SINGLETON.addRunner(r);
            redirect(resp, Const.SUCCESS_PARM, Const.CREATED_MSG);
        } catch (IllegalArgumentException ex) {
            redirect(resp, Const.ERR_PARM, ex.getMessage());
        }
    }

    // ⮝ HttpServlet
    // ⮟ helpers

    private static void redirect(HttpServletResponse resp, String parm, String val) throws IOException {
        resp.sendRedirect(String.format("./%s?%s=%s", Const.ADMIN_SLUG, parm, val));
    }
}
