package amy.fivek.servlets;

import amy.fivek.Const;
import amy.fivek.FiveK;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class StartFiveKServlet extends HttpServlet {

    public static final String REDIRECT_URL = String.format("./%s", Const.ADMIN_SLUG);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        FiveK.SINGLETON.start();
        resp.sendRedirect(REDIRECT_URL);
    }
}
