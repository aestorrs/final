package amy.fivek.servlets;

import amy.fivek.Const;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RedirectHomeServlet extends HttpServlet {

    public static String REDIRECT_URL = String.format("./%s", Const.HOME_SLUG);

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.sendRedirect(REDIRECT_URL);
    }
}
