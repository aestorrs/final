package amy.fivek.servlets;

import amy.fivek.Const;
import amy.fivek.FiveK;
import amy.fivek.Runner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RunnerFinishServlet extends HttpServlet {

    public static final String SUCCESS_MSG = "%s finished with a time of %s";
    public static final String ERR_MSG = "Error: %s";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            Integer id = Integer.valueOf(req.getParameter(Const.ID_PARM));
            FiveK.SINGLETON.finalize(id);
            Runner r = FiveK.SINGLETON.search(id);
            String successMsg = String.format(SUCCESS_MSG, r.getFullName(), r.getFormattedTime());
            resp.sendRedirect(getRedirectURL(Const.SUCCESS_PARM, successMsg));
        } catch (NumberFormatException | IllegalStateException ex) {
            String errorMsg = String.format(ERR_MSG, ex.getMessage());
            resp.sendRedirect(getRedirectURL(Const.ERR_PARM, errorMsg));
        }
    }

    private static String getRedirectURL(String parm, String val) {
        return String.format("./%s?%s=%s", Const.ADMIN_SLUG, parm, val);
    }
}
