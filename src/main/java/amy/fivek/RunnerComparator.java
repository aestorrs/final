package amy.fivek;

import java.util.Comparator;
import java.util.function.Function;

public class RunnerComparator implements Comparator<Runner> {

    private static final char FIRST_NAME = 'f';
    private static final char LAST_NAME = 'l';
    private static final char NUMBER = 'n';
    private static final char TIME = 't';
    private static final char AGE = 'a';
    private static final char GENDER = 'g';
    public static final String NO_ATTR_ERR = "No attribute matched specifier '%c'";

    private final String order;

    public RunnerComparator(String order) {
        this.order = order;
    }

    @Override
    public int compare(Runner r1, Runner r2) {
        int result = 0;

        for (char c : order.toCharArray()) {
            switch (Character.toLowerCase(c)) {
                case FIRST_NAME:
                    result = compareBy(r1, r2, Runner::getFirstName);
                    break;
                case LAST_NAME:
                    result = compareBy(r1, r2, Runner::getLastName);
                    break;
                case NUMBER:
                    result = compareBy(r1, r2, Runner::getNumber);
                    break;
                case TIME:
                    result = compareBy(r1, r2, Runner::getTime);
                    break;
                case AGE:
                    result = compareBy(r1, r2, Runner::getAge);
                    break;
                case GENDER:
                    result = compareBy(r1, r2, Runner::getGender);
                    break;
                default:
                    throw new IllegalArgumentException(String.format(NO_ATTR_ERR, c));
            }

            if (Character.isUpperCase(c)) {
                result = -result;
            }

            if (result != 0) {
                break;
            }
        }

        return result;
    }

    private static <T extends Comparable<T>> int compareBy(Runner r1, Runner r2, Function<Runner, T> getter) {
        Comparable<T> v1 = getter.apply(r1);
        T v2 = getter.apply(r2);

        if (v1 == null && v2 == null) {
            return 0;
        } else if (v1 == null) {
            return 1;
        } else if (v2 == null) {
            return -1;
        } else {
            return v1.compareTo(v2);
        }
    }
}
