package amy.fivek;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FiveK {

    public static final FiveK SINGLETON = new FiveK();


    private final List<Runner> runners = new ArrayList<>();
    private Instant start;

    private FiveK() {
    }

    public void reset() {
        runners.clear();
        start = null;
    }

    public boolean isStarted() {
        return start != null;
    }

    public List<Runner> queryRunners(Predicate<Runner> matcher, String order) {
        return runners.stream()
                .filter(matcher)
                .sorted(new RunnerComparator(order))
                .collect(Collectors.toList());
    }

    public Runner search(Integer id) {
        return queryRunners(r -> r.getId().equals(id), "").get(0);
    }

    public void addRunner(Runner r) {
        runners.add(r);
    }

    public void start() {
        if (!isStarted()) {
            start = Instant.now();
        }
    }

    public void finalize(Integer id) {
        Runner r = search(id);

        if (r == null) {
            throw new IllegalArgumentException(String.format("No Runner found with id=%d", id));
        }

        if (!isStarted()) {
            throw new IllegalStateException("FiveK must start before runners can finish");
        }

        r.setTime(Duration.between(start, Instant.now()));
    }
}
