package amy.fivek;

public class Const {
    public static final String CREATED_MSG = "Runner Created";
    public static final String STARTED_MSG = "FiveK Started at %s";

    public static final String ADMIN_SLUG = "admin";
    public static final String HOME_SLUG = "home";
    public static final String RUNNERS_SLUG = "runners";
    public static final String ADD_RUNNER_SLUG = "add-runner";
    public static final String RUNNER_FINISHED_SLUG = "runner-finish";
    public static final String START_FIVEK_SLUG = "start-five-k";

    public static final String SUCCESS_PARM = "msg";
    public static final String ERR_PARM = "err";
    public static final String FNAME_PARM = "firstName";
    public static final String LNAME_PARM = "lastName";
    public static final String NUM_PARM = "number";
    public static final String AGE_PARM = "age";
    public static final String GENDER_PARM = "gender";
    public static final String ID_PARM = "id";
    public static final String SORT_PARM = "sort";

    public static final String MATCHER_ATTR = "matcher";
    public static final String SHOW_FINISH_BUTTONS_ATTR = "show-finish-buttons";
}
