package amy.fivek;

import java.time.Duration;
import java.util.Comparator;

public class Runner implements Comparable<Runner> {

    public static final int FIRST_NAME_MAX_LENGTH = 20;
    public static final int LAST_NAME_MAX_LENGTH = 20;
    public static final int NUMBER_MAX_LENGTH = 4;
    public static final int AGE_MIN = 0;
    public static final int AGE_MAX = 200;

    public static final String LAST_NAME_CAPTION = "Last Name";
    public static final String FIRST_NAME_CAPTION = "First Name";
    public static final String NUMBER_CAPTION = "Race #";
    public static final String AGE_CAPTION = "Age";
    public static final String GENDER_CAPTION = "Gender";
    public static final String TIME_CAPTION = "Time";

    public static final Comparator<Runner> DEFAULT_COMPARATOR = new RunnerComparator("lf");

    public static final String INTEGER_ATTR_ERR = "%s is required and must be between %d and %d";
    public static final String CHARACTER_ATTR_ERR = "%s is required and must be a single character";
    public static final String STRING_ATTR_ERR = "%s is required and must not exceed %d characters";

    private static int nextId = 0;

    private final Integer id = Runner.nextId++;
    private String firstName;
    private String lastName;
    private String number;
    private Integer age;
    private Character gender;
    private Duration time;

    public Runner(String firstName, String lastName, String number, Integer age, Character gender) {
        setFirstName(firstName);
        setLastName(lastName);
        setNumber(number);
        setAge(age);
        setGender(gender);
        setTime(null);
    }

    // begin getters

    public Integer getId() {
        return id;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getNumber() {
        return number;
    }

    public Integer getAge() {
        return age;
    }

    public Character getGender() {
        return gender;
    }

    public Duration getTime() {
        return time;
    }

    public String getFormattedTime() {
        Duration t = getTime();
        if (t == null) {
            return "";
        }

        long m = t.toMinutes();
        long s = t.getSeconds() - (60 * m);

        return String.format("%02d:%02d", m, s);
    }

    public boolean isRaceComplete() {
        return getTime() != null;
    }

    // end getters
    // begin setters

    private void setFirstName(String firstName) {
        validateStringAttribute(firstName, FIRST_NAME_MAX_LENGTH, FIRST_NAME_CAPTION);
        this.firstName = firstName;
    }

    private void setLastName(String lastName) {
        validateStringAttribute(lastName, LAST_NAME_MAX_LENGTH, LAST_NAME_CAPTION);
        this.lastName = lastName;
    }

    public void setNumber(String number) {
        validateStringAttribute(number, NUMBER_MAX_LENGTH, NUMBER_CAPTION);
        this.number = number;
    }

    public void setAge(Integer age) {
        validateIntegerAttribute(age, AGE_MIN, AGE_MAX, AGE_CAPTION);
        this.age = age;
    }

    public void setGender(Character gender) {
        validateCharacterAttribute(gender, GENDER_CAPTION);
        this.gender = gender;
    }

    public void setTime(Duration time) {
        this.time = time;
    }

    // end setters
    // begin Comparable<>

    @Override
    public int compareTo(Runner o) {
        return DEFAULT_COMPARATOR.compare(this, o);
    }

    // end comparable
    // begin helpers

    private static void validateStringAttribute(String s, Integer maxLength, String caption) throws IllegalArgumentException {
        if (s == null || s.isEmpty() || s.length() > maxLength) {
            String msg = String.format(STRING_ATTR_ERR, caption, maxLength);
            throw new IllegalArgumentException(msg);
        }
    }

    private static void validateIntegerAttribute(Integer i, int min, int max, String caption) {
        if (i == null || i < min || i > max) {
            String msg = String.format(INTEGER_ATTR_ERR, caption, min, max);
            throw new IllegalArgumentException(msg);
        }
    }

    private void validateCharacterAttribute(Character c, String caption) {
        if (c == null) {
            throw new IllegalArgumentException(String.format(CHARACTER_ATTR_ERR, caption));
        }
    }
}
