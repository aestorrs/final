<%@ page import="amy.fivek.FiveK" %>
<%@ page import="amy.fivek.Runner" %>
<%@ page import="java.util.function.Predicate" %>
<%@ page import="amy.fivek.Const" %>

<%
    String sort = request.getParameter(Const.SORT_PARM);
    if (sort == null) {
        sort = "";
    }

    Predicate<Runner> matcher = (Predicate<Runner>) request.getAttribute(Const.MATCHER_ATTR);
    if (matcher == null) {
        matcher = runner -> true;
    }

    Boolean showFinishButtons = (Boolean) request.getAttribute(Const.SHOW_FINISH_BUTTONS_ATTR);
    if (showFinishButtons == null) {
        showFinishButtons = false;
    }
%>

<form class="mui-form" action="./<%= Const.RUNNER_FINISHED_SLUG %>" method="POST">
    <table class="mui-table">
        <thead>
            <tr>
                <th>
                    First Name
                    <a href="./<%= Const.RUNNERS_SLUG %>?<%= Const.SORT_PARM %>=f">
                        <span class="mui-caret"></span>
                    </a>
                    <a href="./<%= Const.RUNNERS_SLUG %>?<%= Const.SORT_PARM %>=F">
                        <span class="mui-caret mui-caret--up"></span>
                    </a>
                </th>
                <th>
                    Last Name
                    <a href="./<%= Const.RUNNERS_SLUG %>?<%= Const.SORT_PARM %>=l">
                        <span class="mui-caret"></span>
                    </a>
                    <a href="./<%= Const.RUNNERS_SLUG %>?<%= Const.SORT_PARM %>=L">
                        <span class="mui-caret mui-caret--up"></span>
                    </a>
                </th>
                <th>
                    Race #
                    <a href="./<%= Const.RUNNERS_SLUG %>?<%= Const.SORT_PARM %>=n">
                        <span class="mui-caret"></span>
                    </a>
                    <a href="./<%= Const.RUNNERS_SLUG %>?<%= Const.SORT_PARM %>=N">
                        <span class="mui-caret mui-caret--up"></span>
                    </a>
                </th>
                <th>
                    Time
                    <a href="./<%= Const.RUNNERS_SLUG %>?<%= Const.SORT_PARM %>=t">
                        <span class="mui-caret"></span>
                    </a>
                    <a href="./<%= Const.RUNNERS_SLUG %>?<%= Const.SORT_PARM %>=T">
                        <span class="mui-caret mui-caret--up"></span>
                    </a>
                </th>
                <th>
                    Age
                    <a href="./<%= Const.RUNNERS_SLUG %>?<%= Const.SORT_PARM %>=a">
                        <span class="mui-caret"></span>
                    </a>
                    <a href="./<%= Const.RUNNERS_SLUG %>?<%= Const.SORT_PARM %>=A">
                        <span class="mui-caret mui-caret--up"></span>
                    </a>
                </th>
                <th>
                    Gender
                    <a href="./<%= Const.RUNNERS_SLUG %>?<%= Const.SORT_PARM %>=g">
                        <span class="mui-caret"></span>
                    </a>
                    <a href="./<%= Const.RUNNERS_SLUG %>?<%= Const.SORT_PARM %>=G">
                        <span class="mui-caret mui-caret--up"></span>
                    </a>
                </th>
<% if (showFinishButtons) { %>
                <th></th>
<% } %>
            </tr>
        </thead>
        <tbody>
<% for (Runner r : FiveK.SINGLETON.queryRunners(matcher, sort)) { %>
            <tr>
                <td><%= r.getFirstName().toString() %></td>
                <td><%= r.getLastName().toString() %></td>
                <td><%= r.getNumber().toString() %></td>
                <td><%= r.getFormattedTime() %></td>
                <td><%= r.getAge().toString() %></td>
                <td><%= r.getGender().toString() %></td>
    <% if (showFinishButtons) { %>
                <td>
                    <button type="submit" class="mui-btn mui-btn--primary mui-btn--small mui-btn--fab mui--align-middle" name="<%= Const.ID_PARM %>" value="<%= r.getId().toString() %>">
                        <img src="https://icon-library.net/images/finish-flag-icon/finish-flag-icon-2.jpg" style="height: 1em">
                    </button>
                </td>
    <% } %>
            </tr>
<% } %>
        </tbody>
    </table>
</form>
