<!DOCTYPE html>
<html>
    <head>
        <%@ include file="fragments/head.jsp" %>
    </head>
    <body>
        <%@ include file="fragments/appbar.jsp" %>
        <main class="mui-container">
            <h2 class="mui-text--center">All Runners</h2>
            <%@ include file="fragments/runners-table.jsp" %>
        </main>
    </body>
</html>
