<%@ page import="amy.fivek.FiveK" %>
<%@ page import="amy.fivek.Runner" %>
<%@ page import="java.util.List" %>

<!DOCTYPE html>
<html>
    <head>
        <%@ include file="fragments/head.jsp" %>
    </head>
    <body>
        <%@ include file="fragments/appbar.jsp" %>
        <main class="mui-container">
            <div class="mui--text-center">
                <img src="http://clipart-library.com/image_gallery/n979798.jpg" />
            </div>
            <h2 class="mui--text-headline mui--text-center">Finishers!</h2>
            <table class="mui-table">
                <thead>
                    <tr>
                        <th></th>
                        <th><%= Runner.TIME_CAPTION %></th>
                        <th>Name</th>
                        <th><%= Runner.NUMBER_CAPTION %></th>
                </thead>
                <tbody>
<%
    int finishRank = 1;
    List<Runner> finishers = FiveK.SINGLETON.queryRunners(Runner::isRaceComplete, "t");
    if (!finishers.isEmpty()) {
        for (Runner r : finishers) {
%>
                    <tr>
                        <td><%= finishRank++ %></td>
                        <td><%= r.getFormattedTime() %></td>
                        <td><%= r.getFullName() %></td>
                        <td><%= r.getNumber() %></td>
                    </tr>
<%
        }
    } else {
%>
                    <tr>
                        <td class="mui--text-center mui--text-dark-secondary" colspan="4">
                            <strong>No one has finished the race yet, go <span class="mui--text-accent">cheer</span> for them!</strong>
                        </td>
                    </tr>
<%
    }
%>
                </tbody>
            </table>
        </main>
    </body>
</html>
